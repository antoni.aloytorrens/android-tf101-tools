PLACE ASUS FIRMWARE ZIP HERE.

DOWNLOAD THE FIRMWARE FOR YOUR COUNTRY.

- UNITED STATES (US FIRMWARE):
	Official: https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/US_epaduser9_2_1_27UpdateLauncher.zip
	Backup: https://web.archive.org/web/20211008162233/https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/US_epaduser9_2_1_27UpdateLauncher.zip

- TAIWAN (TW FIRMWARE):
	Official: https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/TW_epaduser9_2_1_27UpdateLauncher.zip
	Backup: https://web.archive.org/web/20220121013012/https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/TW_epaduser9_2_1_27UpdateLauncher.zip 

- JAPAN (JP FIRMWARE):
	Official: https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/JP_epaduser9_2_1_25UpdateLauncher.zip
	Backup: https://web.archive.org/web/20211008162321/https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/JP_epaduser9_2_1_25UpdateLauncher.zip

- CHINA (CN FIRMWARE):
	Official: https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/CN_epaduser9_2_1_24UpdateLauncher.zip
	Backup: https://web.archive.org/web/20211008162330/https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/CN_epaduser9_2_1_24UpdateLauncher.zip

- OTHERS (WORLDWIDE FIRMWARE):
	Official: https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/WW_epaduser9_2_1_27UpdateLauncher.zip
	Backup: https://web.archive.org/web/20200125091203/https://dlcdnets.asus.com/pub/ASUS/EeePAD/TF101/WW_epaduser9_2_1_27UpdateLauncher.zip

IN MY CASE, IT'S WW_epad-user-9.2.1.27.zip FILE (WORLDWIDE FIRMWARE).
IF YOU HAVE ANY DOUBTS, GO WITH THE WORLDWIDE FIRMWARE.



UNCOMPRESS THE FIRMWARE ZIP FILE.
YOU WILL SEE A BLOB FILE.

THEN, UNCOMPRESS THE BLOB -> Open a terminal and run: `blobunpack blob`

(OPTIONAL)
REMOVE THE BLOB FILE -> rm -r blob
REMOVE THE ZIP FILE -> rm -r *.zip


YOU ARE GOOD TO GO. RUN SBK SH SCRIPT ACCORDING TO YOUR SBK TABLET VERSION.
IN MY CASE IT'S SBKV1.
