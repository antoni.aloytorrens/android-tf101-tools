# Android Asus Eee Pad Transformer TF101 Tools

* **Warning**: These tools won't work on TF101G or SL101 devices!

---

## Setup

Put your TF101 tablet in APX mode (Power + Volume Up and wait 6 seconds) and connect it to your PC using the [USB to 40 Pin cable](https://imgur.com/a/mfiryeP).

* If your tablet is SBKv1 version: run `android_sbkv1.sh` in your PC.
* If your tablet is SBKv2 version: run `android_sbkv2.sh` in your PC.

Put your ASUS .zip firmware (decompressed) on the temporary folder.

* You need to uncompress the zip file first.
* And then uncompress the blob file with `blobunpack blob` command in the terminal. If you don't have the command available, the [sources are here](https://gitlab.com/postmarketOS/asus-transformer-blobtools) and you need to compile it.

## Special Thanks

### Media
- [How to install Debian 11 Bullseye on the Asus Eee Pad Transformer TF101 (Rootbind Method)](https://youtu.be/L2oD4SKdMbo).

### Rootbind
- [jrohwer](https://forum.xda-developers.com/m/jrohwer.3753391/) for its [Rootbind method](https://forum.xda-developers.com/t/kernel-mod-linux-rootbind-native-emmc-all-tf101-tf101g-fast-tested-2-jul-13.2347581/).
- [Kingzak34](https://forum.xda-developers.com/m/kingzak34.3447252/) for its [guide](https://forum.xda-developers.com/t/guide-dual-boot-theeasyway-how-to-setup-linux-rootbind-method-updated-may-16th.2684854/).
- [CrazyPlaysHD](https://github.com/CrazyPlaysHD) for testing and updating Rootbind to the latest Debian release (as of now, Debian 11 Bullseye). [See here](https://github.com/antonialoytorrens/TF101-linux-images/issues/11).
- [Prowler_gr](https://forum.xda-developers.com/m/prowler_gr.2630853/) for testing and updating Rootbind to the latest Devuan release (as of now, Devuan 4 Chimaera). [See here](https://forum.xda-developers.com/t/linux-img-dev-wip-ubuntu-images-for-rootbind-tf101-tf101g.2648862/page-23#post-86260727).

### TWRP

Please look at this thread by sidneyk: https://forum.xda-developers.com/t/recovery-tf101-tf101g-sl101-twrp-2-8-1-1-unofficial-2014-11-06.2434422/

Credits:<br/>
Team Win Recovery Project<br/>
josteink for tf101 device tree<br/>
timduru for tf101 device tree<br/>
josteink for tf101 kernel source<br/>
OmniROM for Android 4.4.2 base<br/>
Google for Android AOSP<br/>
Anyone else who previously contributed to this.
