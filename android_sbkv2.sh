#!/bin/sh

# Check files first
ISINCLUDED=1
. ./checkfiles.sh

# Run process after the files are in place
./wheelie -2 --bl bootloader.bin --bct transformer.bct --odm 0x300d8011 || break
./nvflash -r --bct transformer.bct --setbct --configfile flash.cfg --create --odmdata 0x300d8011 --sync || break
./nvflash -r --go || break
