#!/bin/bash
FILES="temp/blob.APP temp/blob.LNX temp/twrp2811_tf101.SOS temp/blob.EBT"
for file in $FILES;
do
  if [ ! -e "$file" ];
  then
    echo "File $file is missing!";
    HASMISSING=true
  fi 
done

if [ $HASMISSING ];
then
  echo "One or more files are missing. Ensure that blob.APP, blob.LNX, blob.EBT and twrp2811_tf101.SOS all exist in the temp folder to continue!";
  exit 1;
fi
